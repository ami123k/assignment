import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  userList: Array<any> = [];
  constructor() {}

  ngOnInit(): void {
    this.getListOfUser();
  }
  getListOfUser() {
    if (localStorage.userList) {
      this.userList = JSON.parse(localStorage.getItem('userList') || '');
    }
  }
  deleteUser(index: number) {
    this.userList.splice(index, 1);
    localStorage.setItem('userList', JSON.stringify(this.userList));
  }
}

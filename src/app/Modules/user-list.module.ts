import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserListRoutingModule } from './user-list-routing.module';
import { UserListComponent } from './user-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { AddUserComponent } from './add-user/add-user.component';
import { AddressListComponent } from './address-list/address-list.component';

@NgModule({
  declarations: [UserListComponent, AddUserComponent, AddressListComponent],
  imports: [
    CommonModule,
    UserListRoutingModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
})
export class UserListModule {}

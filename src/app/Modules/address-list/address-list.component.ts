import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.scss'],
})
export class AddressListComponent implements OnInit {
  userId: number = 0;
  userList: Array<any> = [];
  addressList: Array<any> = [];
  deleteAll: boolean = false;
  isSelected: boolean = false;
  index: number = 0;
  constructor(private router: ActivatedRoute, private route: Router) {}

  ngOnInit(): void {
    this.router.queryParams.subscribe((res) => {
      if (res) {
        this.userId = Number(res['id']);
        this.index = Number(res['index']);
      }
    });
    this.getUserAddress();
  }

  // Get The list of particular user all Addresses
  getUserAddress() {
    if (localStorage.userList) {
      this.userList = JSON.parse(localStorage.getItem('userList') || '');
      this.userList.filter((item) => {
        if (item.id === this.userId) {
          this.addressList = item.address;
          this.addressList.filter((address) => {
            address['isSelected'] = false;
          });
        }
      });
    }
  }

  // Delete Single Address Row
  deleteRows(index: any) {
    this.addressList.splice(index, 1);
    this.userList[this.index]['address'] = this.addressList;
    localStorage.setItem('userList', JSON.stringify(this.userList));
  }

  // Change All Status of Valid or Invalid Address
  changeStatus(event: any, index: number) {
    this.addressList[index]['status'] = event.checked;
    this.userList[this.index]['address'] = this.addressList;
    localStorage.setItem('userList', JSON.stringify(this.userList));
  }

  // Select All the  rows which you want to delete
  selectedRows(event: any, index: number) {
    this.addressList[index]['isSelected'] = event.checked;
    console.log(this.addressList);

    this.isSelected = this.addressList
      .map((list) => list.isSelected)
      .includes(true)
      ? true
      : false;
  }

  // Delete all selected Rows at once
  deleSelectedRows() {
    let arr: any = [];
    let newArr: any = [];
    console.log(this.addressList);

    this.addressList.filter((item, i) => {
      if (item.isSelected) {
        arr.push(item);
      }
    });
    newArr = this.addressList.filter(
      (add) => !arr.some((arr1: any) => arr1.city === add.city)
    );
    arr.splice(0, arr.length);
    this.addressList = newArr;

    this.userList[this.index]['address'] = newArr;
    localStorage.setItem('userList', JSON.stringify(this.userList));
    this.route.navigate(['']);
  }
}

import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  userForm: FormGroup;
  userList: Array<any> = [];
  submitted: boolean = false;
  id: number = 0;
  userId: number = 0;
  index: number = 0;
  constructor(
    private fb: FormBuilder,
    private route: Router,
    private router: ActivatedRoute
  ) {
    this.userForm = this.fb.group({
      name: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[a-zA-Z ]*$/),
        ])
      ),
      age: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[0-9]*$/),
      ]),
      address: new FormArray([]),
    });
  }

  ngOnInit(): void {
    this.router.queryParams.subscribe((res) => {
      this.userId = Number(res['id']);
      this.index = Number(res['index']);
    });

    // Check If userList is present in LocalStorage
    if (localStorage.userList) {
      this.userList = JSON.parse(localStorage.getItem('userList') || '');
      if (this.userList.length > 0) {
        this.setValues();
        console.log(this.userList);
        this.id = this.userList[this.userList.length - 1]['id'];
      }
    }
  }

  address() {
    return this.userForm.controls['address'] as FormArray;
  }

  // Patch as well as add new controls
  addressForm(item?: any) {
    return this.fb.group({
      city: new FormControl(item ? item.city : '', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z ]*$/),
      ]),
      state: new FormControl(item ? item.state : '', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z ]*$/),
      ]),
      house: new FormControl(item ? item.house : '', [
        Validators.required,
        Validators.pattern(/^[0-9]*$/),
      ]),
      country: new FormControl(item ? item.country : '', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z ]*$/),
      ]),
      status: new FormControl(item ? item.status : false, [
        Validators.required,
      ]),
    });
  }

  //Patch All Values in formcontrol
  setValues() {
    this.userList.filter((item) => {
      if (item.id === this.userId) {
        this.userForm.patchValue(item);
        item.address.filter((address: any) => {
          this.address().push(this.addressForm(address));
        });
      }
    });
  }

  // Push FormGroup into FormArray
  addAddress() {
    this.address().push(this.addressForm());
  }
  submit() {
    this.submitted = true;

    if (this.userForm.valid && this.submitted) {
      const data = {
        ...this.userForm.value,
      };
      if (!this.userId) {
        // In case of Add
        data['id'] = this.id + 1;
        this.userList.push(data);
        console.log(this.userList);
      } else {
        //In case of Edit

        data['id'] = this.userList[this.index]['id'];
        this.userList[this.index] = data;
        console.log(this.userList);
      }
      localStorage.setItem('userList', JSON.stringify(this.userList));
      this.route.navigate(['']);
    }
  }
}
